# Android Job Candidate

### Task Description

Fix all of the TODOs and rewrite the project so it reflects your coding style and preferred way of displaying a list of items and a details page.
We expect that the assignment will be written in Kotlin


<img src="https://gitlab.com/yusufcakmak/storytel-assignment/-/raw/master/resources/storytel.jpg"/>


### Project Details and Development Decisions

I developed this project with Kotlin language and the project based on MVVM architecture. I separated Data, Domain, Repository, and Presentation layers for separation of concern.
Dagger Hilt used for dependency Injection. Dagger hilt implementation is easier than Dagger, documentation is up to date and the learning curve is lower.
Picasso for image loading, I replaced Glide with Picasso because the Glide doesn't work with image URLs. I'm learning Coroutines that's why I chose to use Coroutines over
RxJava also I wanted to show I can learn and implement a new library or technology.

#### Tech Stack

* Kotlin
* Android Architecture Components (ViewModel, LiveData)
* Navigation Component
* DataBinding
* Dagger Hilt
* Kotlin Coroutines
* Retrofit
* Picasso

#### Modules

##### core

The core module is accessible from all other modules since It contains common classes for transporting data between layers.
I could create common-data, common-mapper modules for different purposes, but I wanted to keep it simple.
I am aware that creating a different common module for different purposes is a better approach for a large project.


##### data

The data module is responsible for providing data sources and includes their implementation. We should try to stay away from putting logic inside the data layer.
So Data layer should be as simple as possible to get data from resources. My resource is the only remote data source in this project.
But If we want to persist any data to provide offline support, this is the module we should put it on.


##### domain

The domain module contains all business logic in the app. In this sample app, we have business logic.
We need to combine photos and images. This behavior might be changed in the feature. This is the reason behind having a domain layer.
Domain layer hides the complexity of the app from other layers (From UI, repository, etc.)


##### repository

The repository layer is a decision-maker for the data sources. Since we only have 1 data source (Remote), the repository layer might be seen as unnecessary.
But It provides us the flexibility to add persistence to our app. This is why we have a repository layer.


##### presentation


The presentation layer makes your UI data for us. We should only do UI logic here.


#### Pages And Navigation

The Application contains 3 different pages like below. I use the Android Jetpack Navigation for handling pages' navigation.

* Splash Screen
* Post List Screen
* Comments Screen


<img src="https://gitlab.com/yusufcakmak/storytel-assignment/-/raw/master/resources/navigation.png"/>

For reference -> https://developer.android.com/guide/navigation


### Styling

In this project, I have 3 different TextView styles for title, description, and CTA. Also, I added a style for FAB.


```Title
   <style name="TitleTextStyle" parent="TextAppearance.AppCompat">
        <item name="android:textColor">@color/colorBlack</item>
        <item name="android:textSize">@dimen/title_text_size</item>
        <item name="android:textStyle">bold</item>
    </style>
```

```Description
       <style name="DescriptionTextStyle" parent="TextAppearance.AppCompat">
           <item name="android:textColor">@color/colorBlack</item>
           <item name="android:textSize">@dimen/sub_title_text_size</item>
       </style>
```

```CTA Text
       <style name="CTATextStyle" parent="TextAppearance.AppCompat">
           <item name="android:textColor">@color/colorOrange</item>
           <item name="android:textSize">@dimen/cta_text_size</item>
           <item name="android:textStyle">bold</item>
        </style>
```

```FAB
       <style name="ThemeOverlay.App.FloatingActionButton" parent="">
           <item name="colorSecondary">@color/colorFabBackground</item>
           <item name="colorOnSecondary">@color/colorWhite</item>
           <item name="colorOnSurface">@color/colorFabBackground</item>
       </style>
```

### TODOs && Improvements

* Better Design
* Offline Support
* Dark Theme
* Unit Testing & UI Testing
* Better Error Handling, like 500 Internal Error
* Documentation for every item in tech stack