package app.storytel.candidate.com.presentation.common

import androidx.databinding.ViewDataBinding

inline fun <T : ViewDataBinding> T.execute(block: T.() -> Unit) {
    block()
    executePendingBindings()
}