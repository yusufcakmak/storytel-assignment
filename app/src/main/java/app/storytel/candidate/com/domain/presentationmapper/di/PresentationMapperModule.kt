package app.storytel.candidate.com.domain.presentationmapper.di

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.domain.model.Comment
import app.storytel.candidate.com.domain.model.PostAndImage
import app.storytel.candidate.com.domain.presentationmapper.CommentPresentationMapper
import app.storytel.candidate.com.domain.presentationmapper.PostAndImagePresentationMapper
import app.storytel.candidate.com.presentation.detail.model.CommentUiModel
import app.storytel.candidate.com.presentation.posts.model.PostAndImageUiModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class PresentationMapperModule {

    @Provides
    @Singleton
    fun provideCommentPresentationMapper(): Mapper<Comment, CommentUiModel> {
        return CommentPresentationMapper()
    }

    @Provides
    @Singleton
    fun providePostAndImagePresentationMapper(): Mapper<PostAndImage, PostAndImageUiModel> {
        return PostAndImagePresentationMapper()
    }

}