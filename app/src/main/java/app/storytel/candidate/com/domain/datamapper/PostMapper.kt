package app.storytel.candidate.com.domain.datamapper

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.data.remote.datasource.model.PostResponse
import app.storytel.candidate.com.domain.model.Post
import javax.inject.Inject

class PostMapper @Inject constructor() : Mapper<PostResponse, Post> {

    override fun map(post: PostResponse): Post {
        return Post(
            userId = post.userId,
            id = post.id,
            title = post.title,
            body = post.body
        )
    }
}