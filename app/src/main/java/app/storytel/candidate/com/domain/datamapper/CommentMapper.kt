package app.storytel.candidate.com.domain.datamapper

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.data.remote.datasource.model.CommentResponse
import app.storytel.candidate.com.domain.model.Comment
import javax.inject.Inject

class CommentMapper @Inject constructor() : Mapper<CommentResponse, Comment> {

    override fun map(input: CommentResponse): Comment {
        return Comment(
            postId = input.postId,
            id = input.id,
            name = input.name,
            email = input.email,
            body = input.body
        )
    }

}