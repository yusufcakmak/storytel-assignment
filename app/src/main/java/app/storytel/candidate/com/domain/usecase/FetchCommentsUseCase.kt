package app.storytel.candidate.com.domain.usecase

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.core.Resource
import app.storytel.candidate.com.data.di.IoDispatcher
import app.storytel.candidate.com.domain.model.Comment
import app.storytel.candidate.com.presentation.detail.model.CommentUiModel
import app.storytel.candidate.com.repository.CommentRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class FetchCommentsUseCase @Inject constructor(
    private val repository: CommentRepository,
    private val commentMapper: Mapper<Comment, CommentUiModel>,
    @IoDispatcher val dispatcher: CoroutineDispatcher
) {

    fun fetchComments(postId: Int) = flow {
        emit(Resource.Loading)
        val comments = repository.fetchComments(postId)
        emit(Resource.Success(comments.map {
            commentMapper.map(it)
        }))
    }.catch {
        emit(Resource.Error(it))
    }.flowOn(dispatcher)


}