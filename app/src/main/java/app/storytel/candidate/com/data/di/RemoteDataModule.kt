package app.storytel.candidate.com.data.di

import app.storytel.candidate.com.BuildConfig
import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.data.remote.api.PhotoService
import app.storytel.candidate.com.data.remote.api.PostService
import app.storytel.candidate.com.data.remote.datasource.model.CommentResponse
import app.storytel.candidate.com.data.remote.datasource.model.PhotoResponse
import app.storytel.candidate.com.data.remote.datasource.model.PostResponse
import app.storytel.candidate.com.data.remote.datasource.photos.RemotePhotosDataSource
import app.storytel.candidate.com.data.remote.datasource.photos.RemotePhotosDataSourceImpl
import app.storytel.candidate.com.data.remote.datasource.post.RemotePostDataSource
import app.storytel.candidate.com.data.remote.datasource.post.RemotePostDataSourceImpl
import app.storytel.candidate.com.domain.model.Comment
import app.storytel.candidate.com.domain.model.Photo
import app.storytel.candidate.com.domain.model.Post
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RemoteDataModule {

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun providePostsRestInterface(retrofit: Retrofit): PostService {
        return retrofit.create()
    }

    @Provides
    @Singleton
    fun providePhotoRestInterface(retrofit: Retrofit): PhotoService {
        return retrofit.create()
    }


    @Provides
    @Singleton
    fun provideRemotePostsDataSource(
        api: PostService,
        commentMapper: Mapper<CommentResponse, Comment>,
        postMapper: Mapper<PostResponse, Post>,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ): RemotePostDataSource {
        return RemotePostDataSourceImpl(api, commentMapper, postMapper, dispatcher)
    }

    @Provides
    @Singleton
    fun provideRemotePhotosDataSource(
        api: PhotoService,
        photoMapper: Mapper<PhotoResponse, Photo>,
        @IoDispatcher dispatcher: CoroutineDispatcher
    ): RemotePhotosDataSource {
        return RemotePhotosDataSourceImpl(api, photoMapper, dispatcher)
    }

}
