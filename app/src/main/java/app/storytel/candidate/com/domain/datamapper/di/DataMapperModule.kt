package app.storytel.candidate.com.domain.datamapper.di

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.data.remote.datasource.model.CommentResponse
import app.storytel.candidate.com.data.remote.datasource.model.PhotoResponse
import app.storytel.candidate.com.data.remote.datasource.model.PostResponse
import app.storytel.candidate.com.domain.datamapper.CommentMapper
import app.storytel.candidate.com.domain.datamapper.PhotoMapper
import app.storytel.candidate.com.domain.datamapper.PostMapper
import app.storytel.candidate.com.domain.model.Comment
import app.storytel.candidate.com.domain.model.Photo
import app.storytel.candidate.com.domain.model.Post
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataMapperModule {

    @Provides
    @Singleton
    fun providePhotoMapper(): Mapper<PhotoResponse, Photo> {
        return PhotoMapper()
    }

    @Provides
    @Singleton
    fun provideCommentMapper(): Mapper<CommentResponse, Comment> {
        return CommentMapper()
    }

    @Provides
    @Singleton
    fun providePostMapper(): Mapper<PostResponse, Post> {
        return PostMapper()
    }
}