package app.storytel.candidate.com.presentation.detail

import app.storytel.candidate.com.presentation.posts.model.PostAndImageUiModel

data class PostDetailPageViewState(val postDetail: PostAndImageUiModel?) {

    fun getTitle() = postDetail?.postTitle

    fun getImageUrl() = postDetail?.imageUrl


}
