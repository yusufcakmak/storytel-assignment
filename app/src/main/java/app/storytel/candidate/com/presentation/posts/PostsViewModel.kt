package app.storytel.candidate.com.presentation.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.storytel.candidate.com.core.doOnStatusChanged
import app.storytel.candidate.com.core.doOnSuccess
import app.storytel.candidate.com.domain.usecase.FetchPostsAndPhotosUseCase
import app.storytel.candidate.com.presentation.common.StatusViewState
import app.storytel.candidate.com.presentation.posts.model.PostAndImageUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import javax.inject.Inject

@HiltViewModel
class PostsViewModel @Inject constructor(
    private val fetchPostsUseCase: FetchPostsAndPhotosUseCase
) : ViewModel() {

    private val contents = MutableLiveData<List<PostAndImageUiModel>>()
    val contents_: LiveData<List<PostAndImageUiModel>> = contents

    private val status = MutableLiveData<StatusViewState>()
    val status_: LiveData<StatusViewState> = status

    init {
        fetchPosts()
    }

    fun fetchPosts() {
        fetchPostsUseCase
            .fetchPosts()
            .doOnSuccess { data ->
                contents.value = data
            }
            .doOnStatusChanged {
                status.value = StatusViewState(status = it)
            }
            .launchIn(viewModelScope)
    }
}
