package app.storytel.candidate.com.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import app.storytel.candidate.com.core.doOnStatusChanged
import app.storytel.candidate.com.core.doOnSuccess
import app.storytel.candidate.com.domain.usecase.FetchCommentsUseCase
import app.storytel.candidate.com.presentation.common.StatusViewState
import app.storytel.candidate.com.presentation.detail.model.CommentUiModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import javax.inject.Inject

@HiltViewModel
class PostDetailViewModel @Inject constructor(
    private val fetchCommentsUseCase: FetchCommentsUseCase
) : ViewModel() {
    private val comments = MutableLiveData<List<CommentUiModel>>()
    val comments_: LiveData<List<CommentUiModel>> = comments

    private val status = MutableLiveData<StatusViewState>()
    val status_: LiveData<StatusViewState> = status


    fun fetchPosts(postId: Int) {
        fetchCommentsUseCase
            .fetchComments(postId)
            .doOnSuccess { data ->
                comments.value = data
            }
            .doOnStatusChanged {
                status.value = StatusViewState(status = it)
            }
            .launchIn(viewModelScope)
    }

}