package app.storytel.candidate.com.presentation.posts

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.FragmentPostsBinding
import app.storytel.candidate.com.databinding.PostItemBinding
import app.storytel.candidate.com.presentation.base.BaseFragment
import app.storytel.candidate.com.presentation.common.BindingListAdapter
import app.storytel.candidate.com.presentation.common.StatusViewState
import app.storytel.candidate.com.presentation.common.execute
import app.storytel.candidate.com.presentation.detail.PostDetailFragment
import app.storytel.candidate.com.presentation.posts.model.PostAndImageUiModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostFragment : BaseFragment<FragmentPostsBinding>() {

    private val postsViewModel: PostsViewModel by viewModels()

    override val layoutRes: Int = R.layout.fragment_posts

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initPostsRv()
        observerPosts()
        initSwipeRefreshLayout()

    }

    private fun observerPosts() {
        postsViewModel.status_.observe(viewLifecycleOwner, {
            handleStatus(it)
        })

        postsViewModel.contents_.observe(viewLifecycleOwner, {
            if (it.isNotEmpty()) {
                initPostsData(it)
            }
        })
    }

    private fun handleStatus(statusViewState: StatusViewState) {

        // dont assign here move to to xml
        binding.contentLayout.swipeRefreshLayout.isRefreshing = false

        binding.errorLayout.tvRetry.setOnClickListener {
            postsViewModel.fetchPosts()
        }

        binding.viewState = statusViewState
        binding.executePendingBindings()
    }


    private fun initPostsRv() = with(binding.contentLayout.rvPosts) {
        adapter = postsAdapter
    }

    private fun initPostsData(list: List<PostAndImageUiModel>) {
        postsAdapter.submitList(list)
    }

    private fun initSwipeRefreshLayout() {
        binding.contentLayout.swipeRefreshLayout.setOnRefreshListener {
            postsViewModel.fetchPosts()

            // dont assign here move to to xml
            binding.contentLayout.swipeRefreshLayout.isRefreshing = true
        }

    }

    private val postsAdapter by lazy {
        BindingListAdapter<PostAndImageUiModel, PostItemBinding>(R.layout.post_item) {
            onBind { post, _ ->
                binding.execute {
                    viewData = post
                    executePendingBindings()
                }
            }

            onClick {
                findNavController()
                    .navigate(PostFragmentDirections.actionFragmentPostsToFragmentPostDetail(it))
            }
        }
    }

}