package app.storytel.candidate.com.data.remote.api

import app.storytel.candidate.com.data.remote.datasource.model.CommentResponse
import app.storytel.candidate.com.data.remote.datasource.model.PostResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface PostService {

    @GET("/posts")
    suspend fun fetchPosts(): List<PostResponse>

    @GET("/posts/{id}/comments")
    suspend fun fetchComments(@Path("id") id: Int?) : List<CommentResponse>
}