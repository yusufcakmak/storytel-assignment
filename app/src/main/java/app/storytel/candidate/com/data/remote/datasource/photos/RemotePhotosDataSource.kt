package app.storytel.candidate.com.data.remote.datasource.photos

import app.storytel.candidate.com.domain.model.Photo


interface RemotePhotosDataSource {

    suspend fun fetchRemotePhotos() : List<Photo>

}