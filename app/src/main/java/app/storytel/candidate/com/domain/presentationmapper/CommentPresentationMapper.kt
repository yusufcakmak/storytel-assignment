package app.storytel.candidate.com.domain.presentationmapper

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.domain.model.Comment
import app.storytel.candidate.com.presentation.detail.model.CommentUiModel
import javax.inject.Inject

class CommentPresentationMapper @Inject constructor() : Mapper<Comment, CommentUiModel> {

    override fun map(input: Comment): CommentUiModel {
        return CommentUiModel(
            name = input.name,
            email = input.email,
            body = input.body
        )
    }
}