package app.storytel.candidate.com.data.remote.datasource.photos

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.data.di.IoDispatcher
import app.storytel.candidate.com.data.remote.api.PhotoService
import app.storytel.candidate.com.data.remote.datasource.model.PhotoResponse
import app.storytel.candidate.com.domain.model.Photo
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RemotePhotosDataSourceImpl @Inject constructor(
    private val api: PhotoService,
    private val photoMapper: Mapper<PhotoResponse, Photo>,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : RemotePhotosDataSource {

    override suspend fun fetchRemotePhotos(): List<Photo> = withContext(dispatcher) {
        api.fetchPhotos().map {
            photoMapper.map(it)
        }
    }
}