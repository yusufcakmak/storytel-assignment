package app.storytel.candidate.com.presentation.splash

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.FragmentSplashBinding
import app.storytel.candidate.com.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashFragment : BaseFragment<FragmentSplashBinding>() {

    override val layoutRes: Int = R.layout.fragment_splash

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        lifecycleScope.launch {
            delay(TIME_MILIS_SPLASH)
            navigateToPosts()
        }
    }

    private fun navigateToPosts() {
        findNavController().navigate(R.id.action_fragment_splash_to_fragment_posts)
    }

    companion object {
        private const val TIME_MILIS_SPLASH = 2000L
    }
}