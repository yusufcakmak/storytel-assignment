package app.storytel.candidate.com.data.remote.datasource.post

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.data.di.IoDispatcher
import app.storytel.candidate.com.data.remote.api.PostService
import app.storytel.candidate.com.data.remote.datasource.model.CommentResponse
import app.storytel.candidate.com.data.remote.datasource.model.PostResponse
import app.storytel.candidate.com.domain.model.Comment
import app.storytel.candidate.com.domain.model.Post
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RemotePostDataSourceImpl @Inject constructor(
    private val api: PostService,
    private val commentMapper: Mapper<CommentResponse, Comment>,
    private val postMapper: Mapper<PostResponse, Post>,
    @IoDispatcher private val dispatcher: CoroutineDispatcher
) : RemotePostDataSource {

    override suspend fun fetchPosts(): List<Post> = withContext(dispatcher) {
        api.fetchPosts().map {
            postMapper.map(it)
        }
    }

    override suspend fun fetchComments(id: Int): List<Comment> = withContext(dispatcher) {
        api.fetchComments(id).map {
            commentMapper.map(it)
        }
    }

}