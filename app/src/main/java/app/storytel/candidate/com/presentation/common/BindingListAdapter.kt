package app.storytel.candidate.com.presentation.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class BindingListAdapter<T : Any, DB : ViewDataBinding>(@LayoutRes private val layoutId: Int,
                                                        diffCallback: DiffUtil.ItemCallback<T> = defaultDiffCallback(),
                                                        private val viewHolderBlock: ListAdapterViewHolder<T, DB>.() -> Unit
) : ListAdapter<T, ListAdapterViewHolder<T, DB>>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListAdapterViewHolder<T, DB> {
        val binding: DB = DataBindingUtil.inflate(LayoutInflater.from(parent.context), layoutId, parent, false)
        return ListAdapterViewHolder<T, DB>(binding).apply(viewHolderBlock)
    }

    override fun onBindViewHolder(holder: ListAdapterViewHolder<T, DB>, position: Int) {
        holder.run {
            item = currentList[position]
            binding.execute {
                onBind?.invoke(item, position)
            }
        }
    }

    companion object {
        private fun <T> defaultDiffCallback() = object : DiffUtil.ItemCallback<T>() {
            override fun areItemsTheSame(oldItem: T, newItem: T) = oldItem == newItem
            override fun areContentsTheSame(oldItem: T, newItem: T) = true
        }
    }
}

class ListAdapterViewHolder<T : Any, DB : ViewDataBinding>(val binding: DB) : RecyclerView.ViewHolder(binding.root) {
    internal lateinit var item: T
    internal var onBind: ((item: T, position: Int) -> Unit)? = null

    fun onBind(block: (item: T, position: Int) -> Unit) {
        onBind = block
    }

    fun onClick(block: (item: T) -> Unit) {
        binding.root.setOnClickListener {
            block(item)
        }
    }

}