package app.storytel.candidate.com.presentation.posts.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PostAndImageUiModel(
    val postTitle : String?,
    val postBody : String?,
    val postId : Int?,
    val thumbnailUrl: String?,
    val imageUrl : String?
) : Parcelable
