package app.storytel.candidate.com.domain.model

data class PostAndImage(
    val post: Post?,
    val thumbnailUrl: String?,
    val imageUrl : String?
)