package app.storytel.candidate.com.domain.datamapper

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.data.remote.datasource.model.PhotoResponse
import app.storytel.candidate.com.domain.model.Photo
import javax.inject.Inject

class PhotoMapper @Inject constructor() : Mapper<PhotoResponse, Photo> {

    override fun map(photoResponse: PhotoResponse): Photo {
        return Photo(
            albumId = photoResponse.albumId,
            id = photoResponse.id,
            title = photoResponse.title,
            url = photoResponse.url,
            thumbnailUrl = photoResponse.thumbnailUrl
        )
    }
}