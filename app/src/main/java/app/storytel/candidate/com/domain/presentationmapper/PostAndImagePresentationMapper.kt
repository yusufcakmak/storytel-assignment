package app.storytel.candidate.com.domain.presentationmapper

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.domain.model.Post
import app.storytel.candidate.com.domain.model.PostAndImage
import app.storytel.candidate.com.presentation.posts.model.PostAndImageUiModel
import app.storytel.candidate.com.presentation.posts.model.PostUiModel
import javax.inject.Inject

class PostAndImagePresentationMapper @Inject constructor() :
    Mapper<PostAndImage, PostAndImageUiModel> {

    override fun map(input: PostAndImage): PostAndImageUiModel {
        return PostAndImageUiModel(
            postId = input.post?.id,
            postTitle = input.post?.title,
            postBody = input.post?.body,
            thumbnailUrl = input.thumbnailUrl,
            imageUrl = input.imageUrl
        )
    }

}