package app.storytel.candidate.com.presentation.posts.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PostUiModel(
    val id: Int?,
    val title: String?,
    val body: String?
): Parcelable
