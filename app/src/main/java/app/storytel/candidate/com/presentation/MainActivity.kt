package app.storytel.candidate.com.presentation

import android.os.Bundle
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.ActivityMainBinding
import app.storytel.candidate.com.presentation.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

/*
Hilt can provide dependencies to other Android classes that have the @AndroidEntryPoint annotation
*/

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {

    override val layoutRes: Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}