package app.storytel.candidate.com.data.remote.datasource.post

import app.storytel.candidate.com.domain.model.Comment
import app.storytel.candidate.com.domain.model.Post

interface RemotePostDataSource {

    suspend fun fetchPosts(): List<Post>

    suspend fun fetchComments(id: Int): List<Comment>
}