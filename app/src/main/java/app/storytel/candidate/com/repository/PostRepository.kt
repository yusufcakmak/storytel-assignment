package app.storytel.candidate.com.repository

import app.storytel.candidate.com.data.remote.datasource.photos.RemotePhotosDataSource
import app.storytel.candidate.com.data.remote.datasource.post.RemotePostDataSource
import javax.inject.Inject

class PostRepository @Inject constructor(
    private val remotePostDataSource: RemotePostDataSource,
    private val remotePhotosDataSource: RemotePhotosDataSource,
) {
    suspend fun fetchPhotos() = remotePhotosDataSource.fetchRemotePhotos()

    suspend fun fetchPosts() = remotePostDataSource.fetchPosts()

}