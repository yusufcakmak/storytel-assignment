package app.storytel.candidate.com.repository

import app.storytel.candidate.com.data.remote.datasource.post.RemotePostDataSource
import javax.inject.Inject

class CommentRepository @Inject constructor(
    private val remotePostDataSource: RemotePostDataSource
) {
    suspend fun fetchComments(postId: Int) = remotePostDataSource.fetchComments(postId)
}