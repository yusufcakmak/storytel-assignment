package app.storytel.candidate.com.presentation.detail.model

data class CommentUiModel(
    val name: String?,
    val email: String?,
    val body: String?,
)