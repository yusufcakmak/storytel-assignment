package app.storytel.candidate.com.domain.usecase

import app.storytel.candidate.com.core.Mapper
import app.storytel.candidate.com.core.Resource
import app.storytel.candidate.com.data.di.IoDispatcher
import app.storytel.candidate.com.domain.model.Photo
import app.storytel.candidate.com.domain.model.Post
import app.storytel.candidate.com.domain.model.PostAndImage
import app.storytel.candidate.com.presentation.posts.model.PostAndImageUiModel
import app.storytel.candidate.com.repository.PostRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FetchPostsAndPhotosUseCase @Inject constructor(
    private val repository: PostRepository,
    private val postAndImageMapper: Mapper<PostAndImage, PostAndImageUiModel>,
    @IoDispatcher val dispatcher: CoroutineDispatcher
) {

    fun fetchPosts() = flow {
        emit(Resource.Loading)
        emit(Resource.Success(fetchPhotosAndPosts()))
    }.catch { ex ->
        emit(Resource.Error(ex))
    }.flowOn(dispatcher)


    private suspend fun fetchPhotosAndPosts() = withContext(dispatcher) {
        val photos = async { repository.fetchPhotos() }
        val posts = async { repository.fetchPosts() }

        combinePostsAndPhotos(posts.await(), photos.await()).map {
            postAndImageMapper.map(it)
        }
    }

    private fun combinePostsAndPhotos(
        postList: List<Post>,
        photos: List<Photo>
    ): List<PostAndImage> {
        return postList.map {
            val selectedPhoto = photos.random()
            PostAndImage(
                post = it,
                thumbnailUrl = selectedPhoto.thumbnailUrl,
                imageUrl = selectedPhoto.url
            )
        }

    }
}