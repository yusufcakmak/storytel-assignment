package app.storytel.candidate.com.core

interface Mapper<In, Out> {

    fun map(input: In): Out

}