package app.storytel.candidate.com.presentation.detail

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.app.ShareCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import app.storytel.candidate.com.R
import app.storytel.candidate.com.databinding.CommentItemBinding
import app.storytel.candidate.com.databinding.FragmentPostDetailBinding
import app.storytel.candidate.com.presentation.base.BaseFragment
import app.storytel.candidate.com.presentation.common.BindingListAdapter
import app.storytel.candidate.com.presentation.common.StatusViewState
import app.storytel.candidate.com.presentation.common.execute
import app.storytel.candidate.com.presentation.detail.model.CommentUiModel
import app.storytel.candidate.com.presentation.posts.model.PostAndImageUiModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PostDetailFragment : BaseFragment<FragmentPostDetailBinding>() {

    override val layoutRes: Int = R.layout.fragment_post_detail

    private val postDetailViewModel: PostDetailViewModel by viewModels()

    private val detailArgs by navArgs<PostDetailFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (detailArgs.postItem?.postId == null)
            return
        else
            postDetailViewModel.fetchPosts(detailArgs.postItem?.postId!!)

        initPostData(detailArgs.postItem)
        observePostDetail()
        initCommentsAdapter()
    }

    private fun initPostData(postUi: PostAndImageUiModel?) {
        binding.viewData = PostDetailPageViewState(postUi)
        binding.layoutDetailContent.fabEmail.setOnClickListener {
            Toast.makeText(requireContext(), getString(R.string.hello_storytel), Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun observePostDetail() {
        postDetailViewModel.comments_.observe(viewLifecycleOwner, {
            commentsAdapter.submitList(it)
        })

        postDetailViewModel.status_.observe(viewLifecycleOwner, {
            handleStatus(it)
        })
    }

    private fun handleStatus(statusViewState: StatusViewState) {
        binding.viewState = statusViewState
        binding.executePendingBindings()

        binding.errorLayout.tvRetry.setOnClickListener {
            detailArgs.postItem?.postId?.let { it1 -> postDetailViewModel.fetchPosts(it1) }
        }
    }

    private fun initCommentsAdapter() {
        binding.layoutDetailContent.rvComments.adapter = commentsAdapter
    }

    private val commentsAdapter by lazy {
        BindingListAdapter<CommentUiModel, CommentItemBinding>(R.layout.comment_item) {
            onBind { post, _ ->
                binding.execute {
                    viewData = post
                    executePendingBindings()
                }
            }
        }
    }
}